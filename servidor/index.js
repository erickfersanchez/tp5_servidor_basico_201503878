require('dotenv').config();

const express = require('express');
const app = express();
const cors = require('cors');
const PORT = 3000;

app.use(cors());
app.use(express.json({urlencoded: false}));

app.get('/', (req, res) => {
    const json = "Software Avanzado - Tarea Práctica 5 - 201503878 - Erick Fernando Sanchez Mejia”";
    res.status(200).json(json);
});

app.listen(process.env.PORT || PORT, () => {
    console.log(`Web server started at ${process.env.PORT || PORT}...`);
});